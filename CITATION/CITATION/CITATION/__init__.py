import os
from json import dumps
from flask import Flask, render_template, Response, g, request
from neo4j import GraphDatabase, basic_auth
from datetime import date
import logging
import time
app = Flask(__name__)
database = os.getenv("NEO4J_DATABASE", "TESTDB")
driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "123"))
session = driver.session()

neo4jVersion = os.getenv("NEO4J_VERSION", "")

def get_db():
    if not hasattr(g, 'neo4j_db'):
        g.neo4j_db = driver.session()
    return g.neo4j_db


@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'neo4j_db'):
        g.neo4j_db.close()


@app.route("/getpaper")
def get_paper():
    start_time = time.time()
    try:
        q = request.args["tt"]
        t = request.args["tp"]
        a = request.args["au"]
    except KeyError:
        q = " "
        t = " "
        a = " "
    else:
        db = get_db()
        if q == '':
            title = "'(?i).* .*')"
        else:
            title = "'(?i).*" + q + ".*')"
        if t == '':
            topic =  ''
        else:
            topic = "and p.topic_name = '" + t + "'"
        if a == '':
            author =  ''
        else:
            author = "and '" + a + "' in p.authors"
      
        query = " MATCH (p:Paper)  WHERE toLower(p.title) =~ toLower(" + title + topic + author + " RETURN p ORDER BY p.year DESC LIMIT 1000"
        results1 = db.read_transaction(lambda tx: list(tx.run(query)))
        end_time = time.time() - start_time
        return Response(dumps([gen_paper(record['p']) for record in results1]),
                    mimetype="application/json")


@app.route("/getbytopic")
def get_by_topic():
    start_time = time.time()
    try:
        t = request.args["tp"]
        o = request.args["order"]
    except KeyError:
        t = " "
        o = '0'
    else:
        db = get_db()
        if t == '':
            topic = "'(?i).* .*')"
        else:
            topic = "'(?i).*" + t + ".*')"
      
        if o == '0':
            query = " MATCH (p:Paper)  WHERE toLower(p.topic_name) =~ toLower(" + topic + " RETURN p ORDER BY p.year DESC LIMIT 100"
        else:
            query = " MATCH c=()-[r:Cities]-(p:Paper)  WHERE toLower(p.topic_name) =~ toLower(" + topic + " RETURN p, count(r) as tong ORDER BY tong DESC LIMIT 100"

      
        results1 = db.read_transaction(lambda tx: list(tx.run(query)))
        end_time = time.time() - start_time
        return Response(dumps([gen_paper(record['p']) for record in results1]),
                    mimetype="application/json")


@app.route("/getbyauthor")
def get_by_author():
    start_time = time.time()
    try:
        t = request.args["tp"]
        o = request.args["order"]
    except KeyError:
        t = " "
        o = '0'
    else:
        db = get_db()
        if t == '':
            topic = "'(?i).* .*')"
        else:
            topic = "'(?i).*" + t + ".*')"
      
        if o == '0':
            query = " MATCH c=(a:Author)-[r:Compose]->(p:Paper)  WHERE toLower(a.author_name) =~ toLower(" + topic + " RETURN p ORDER BY p.year DESC LIMIT 100"
        else:
            query = " MATCH c=(a:Author)-[r:Compose]->(p:Paper)<-[g:Cities]-()  WHERE toLower(a.author_name) =~ toLower(" + topic + " RETURN p, count(g) as tong ORDER BY tong DESC LIMIT 100"

      
        results1 = db.read_transaction(lambda tx: list(tx.run(query)))
        end_time = time.time() - start_time
        return Response(dumps([gen_paper(record['p']) for record in results1]),
                    mimetype="application/json")



@app.route("/getauthor")
def get_author():
    try:
        a = request.args["au"]
    except KeyError:
        a = " "
    else:
        db = get_db()
        if a == '':
            name = "'(?i).* .*')"
        else:
            name = "'(?i).*" + a + ".*')"
      
        query = " MATCH (p:Author)  WHERE toLower(p.author_name) =~ toLower(" + name + "RETURN p  ORDER BY p.author_name  LIMIT 30"
        results1 = db.read_transaction(lambda tx: list(tx.run(query)))
        return Response(dumps([gen_suggest_author(record['p']) for record in results1]),
                    mimetype="application/json")



@app.route("/gettopic")
def get_topic():
    db = get_db()
    results = db.read_transaction(lambda tx: list(tx.run("MATCH (p:Topic) RETURN p")))
    return Response(dumps([gen_topic(record['p']) for record in results]),
                    mimetype="application/json")


@app.route("/")
def main():
    return render_template('index.html')


@app.route("/paper/<id>")
def paper(id):
    start_time = time.time()
    db = get_db()
    paper = db.read_transaction(lambda tx: tx.run("MATCH (p:Paper {paper_id:$paper_id}) "
                                                   "RETURN p", {"paper_id": id}).single())
    
    data_citing = []
    citing = db.read_transaction(lambda tx: list(tx.run("MATCH (p1:Paper)-[r:Cities]->(p2:Paper) where p1.paper_id ='"+ id+"' RETURN p2")))
    for ct in citing:
        data_citing.append(ct['p2'])

    data_cited = []
    citied = db.read_transaction(lambda tx: list(tx.run("MATCH (p1:Paper)-[r:Cities]->(p2:Paper) where p2.paper_id ='"+ id+"' RETURN p1 ORDER BY p1.year DESC LIMIT 100")))
    for ct in citied:
        data_cited.append(ct['p1'])

    lien_quan = []
    chu_de = paper[0]['topic_name']
    lq = db.read_transaction(lambda tx: list(tx.run("MATCH (p:Paper) where p.topic_name ='"+ chu_de +"' RETURN p ORDER BY p.year DESC LIMIT 100")))
    for ct in lq:
        lien_quan.append(ct['p'])

    relate = []
    chu_de = paper[0]['topic_name']
    rl = db.read_transaction(lambda tx: list(tx.run("MATCH (p1:Paper)-[r:Cities]->(p2:Paper) where p1.paper_id ='"+ id+"' and p2.topic_name ='"+ chu_de +"' RETURN p2 ")))
    rl2 = db.read_transaction(lambda tx: list(tx.run("MATCH (p1:Paper)-[r:Cities]->(p2:Paper) where p2.paper_id ='"+ id+"' and p1.topic_name ='"+ chu_de +"' RETURN p1 ")))
    for ct in rl:
        relate.append(ct['p2'])
    for ct in rl2:
        relate.append(ct['p1'])
    nodes = []
    rels = []
    data_check = []
    author_check = []


    def find(arr , id):
        g = -1
        for x in arr:
            if x['paper_id'] == id:
                g = x['id']
                return g
        return g


    def find_author(arr , id):
        g = -1
        for x in arr:
            if x['name'] == id:
                g = x['id']
                return g
        return g


    i = 0
    title = str(paper[0]['year']) + '_' + paper[0]['title'][0:20]  + '...(' + paper[0]['topic_name'] + ')'
    nodes.append({ "id": i , "title":  title, "root": 'true' ,"type": "goc", })
    target = i
    i += 1
    for name in paper[0]["authors"]:
        actor = {"id" : i, "title":  name, "type": "tacgia", "cluster": "1", "root": 'true',  "caption" : paper[0]['title']}
        try:
            source = nodes.index(actor)
        except ValueError:
            nodes.append(actor)
            source = i
            author_check.append({"id" : i,"name" : name})
            i += 1
        rels.append({"source": source, "target": target, "relatedness": "composed"})    
         
    for pp in data_citing:
        title = str(pp['year']) + '_' + pp['title'][0:20]  + '...(' + pp['topic_name'] + ')'
        citing_node = {"id" : i, "title":  title,  "cluster": "2",  "caption" : pp['title'], "type": "trichdan", }
        data_check.append({"id": i, "paper_id": pp['paper_id']})
        try:
            source = nodes.index(citing_node)
        except ValueError:
            nodes.append(citing_node)
            source = i
            for name in pp["authors"]:
                source_au = find_author(author_check, name)
                if source_au != -1:
                    rels.append({"source": source_au, "target": i , "relatedness": "composed", "style" : "reds"}) 
            i += 1
        rels.append({"source": target, "target": source, "relatedness": "citing"})     

       
        
    i += 1
    for pp in data_cited:
        title = str(pp['year']) + '_' + pp['title'][0:20]  + '...(' + pp['topic_name'] + ')'
        cited_node = {"id" : i, "title":  title,  "cluster": "3",   "caption" : pp['title'] , "type": "duoctrichdan", }
        data_check.append({"id": i, "paper_id": pp['paper_id']})
        try:
            source = nodes.index(cited_node)
        except ValueError:
            nodes.append(cited_node)
            source = i
            for name in pp["authors"]:
                source_au = find_author(author_check, name)
                if source_au != -1:
                    rels.append({"source": source_au, "target": i, "relatedness": "composed", "type" : "reds"}) 

            i += 1
        rels.append({"source": source, "target":target , "relatedness": "cited"})
     

    for pp in data_citing:
        for name in pp["references"]:
            source = find(data_check, pp['paper_id'])
            target = find(data_check, name)
            if target != -1:
                rels.append({"source": source, "target": target, "relatedness": "citing",  "type": "all"}) 
     
            
    for pp in data_cited:
        for name in pp["references"]:
            source = find(data_check, pp['paper_id'])
            target = find(data_check, name)
            if target != -1:
                rels.append({"source": source, "target": target, "relatedness": "citing",  "type": "all",}) 


    graph =  dumps({"nodes": nodes, "edges": rels})
    bieudore = bieu_do_re(id)
    bieudotron = bieu_do_chu_de(id)
    end_time = time.time() - start_time
   
    return render_template('paper.html', data = paper, citing = data_citing, cited = data_cited, lienquan = lien_quan , graph = graph, relate = relate, bieudore = dumps(bieudore) , bieudotron = dumps(bieudotron) )


def bieu_do_re(id):
    db = get_db()
    bieudore =[]
    paper = db.read_transaction(lambda tx: tx.run("MATCH (p:Paper {paper_id:$paper_id}) "
                                                   "RETURN p", {"paper_id": id}).single())
    nam = int(paper[0]['year'])
    todays_date = date.today()
    for x in range(nam,todays_date.year):
        citied = db.read_transaction(lambda tx: list(tx.run("MATCH (p1:Paper)-[r:Cities]->(p2:Paper) where p2.paper_id ='"+ id+"' and p1.year ="+ str(x) +" RETURN count(p1) ")))
        bieudore.append({'x':x, 'y':  citied[0]['count(p1)']})
    return bieudore


def bieu_do_chu_de(id):
    db = get_db()
    total = db.read_transaction(lambda tx: tx.run("MATCH c=(a:Paper)-[r:Cities]->(p:Paper)  where  p.paper_id ='"+ id+ "' RETURN count(p) ").single())
    phan_tram = db.read_transaction(lambda tx: list(tx.run("MATCH c=(t:Paper)-[r:Cities]->(p:Paper) where  p.paper_id ='"+ id+ "' RETURN  DISTINCT t.topic_name ")))
    do_tron = []
    for pt in phan_tram:
        data_pp = db.read_transaction(lambda tx: tx.run("MATCH c=(a:Paper)-[r:Cities]->(p:Paper)where  p.paper_id ='"+ id+ "' and a.topic_name = '"+ pt[0] +"' RETURN count(p) ").single())
        ti_le = int(data_pp[0]) * 100 / int(total[0])
        do_tron.append({'y': ti_le, 'label': pt[0]})
    return do_tron

@app.route("/authors/<name>")
def author(name):
    start_time = time.time()
    db = get_db()
    author = db.read_transaction(lambda tx: tx.run("MATCH (p:Author {author_name:$author_name}) "
                                                   "RETURN p", {"author_name": name}).single())    
    
    total = db.read_transaction(lambda tx: tx.run("MATCH c=(a:Author)-[r:Compose]->(p:Paper) where a.author_name = $author_name"
                                                  " RETURN count(p) ", {"author_name": name}).single())
    
    paper = db.read_transaction(lambda tx: list(tx.run("MATCH c=(t:Author)-[r:Compose]->(p:Paper) where t.author_name = $author_name"
                                                  " RETURN p ORDER BY p.year DESC  limit 100 ", {"author_name": name})))

    phan_tram = db.read_transaction(lambda tx: list(tx.run("MATCH c=(t:Author)-[r:Compose]->(p:Paper) where t.author_name = $author_name"
                                                  " RETURN  DISTINCT p.topic_name ", {"author_name": name})))
    do_tron = []
    for pt in phan_tram:
        data_pp = db.read_transaction(lambda tx: tx.run("MATCH c=(a:Author)-[r:Compose]->(p:Paper) where a.author_name = '"+name+"' and p.topic_name = '"+ pt[0] +"' RETURN count(p) ").single())
        ti_le = int(data_pp[0]) * 100 / int(total[0])
        do_tron.append({'name': pt[0], 'y': ti_le})
    bieu_do = []
    for i in range(10):
        year = 2011 + i;
        data_pp = db.read_transaction(lambda tx: tx.run("MATCH c=(a:Author)-[r:Compose]->(p:Paper) where a.author_name = '"+name+"' and p.year = "+str(year)+" RETURN count(p) ").single())
        bieu_do.append({'label': str(year), 'y': data_pp[0]})
    end_time = time.time() - start_time
    return render_template('author.html',data = author, total = total, paper = paper, bieu_do =  dumps(bieu_do), do_tron = dumps(do_tron) )


@app.route("/topic/<name>")
def topic(name):
    start_time = time.time()
    db = get_db()
    topic = db.read_transaction(lambda tx: tx.run("MATCH (p:Topic {topic_name:$topic_name}) "
                                                   "RETURN p", {"topic_name": name}).single())    
    
    total = db.read_transaction(lambda tx: tx.run("MATCH c=(t:Topic)-[r:THEME]->(p:Paper) where t.topic_name = $topic_name"
                                                  " RETURN count(p) ", {"topic_name": name}).single())
    
    paper = db.read_transaction(lambda tx: list(tx.run("MATCH c=(t:Topic)-[r:THEME]->(p:Paper) where t.topic_name = $topic_name"
                                                  " RETURN p ORDER BY p.year DESC  limit 100 ", {"topic_name": name})))
    bieu_do = []
    for i in range(10):
        year = 2011 + i;
        data_pp = db.read_transaction(lambda tx: tx.run("MATCH (p:Paper) where p.topic_name = '"+name+"' and p.year = "+str(year)+" RETURN count(p) ").single())
        bieu_do.append({'label': str(year), 'y': data_pp[0]})
    end_time = time.time() - start_time
    return render_template('topic.html',data = topic, total = total, paper = paper, bieu_do =  dumps(bieu_do))



def gen_paper(paper):
    return {
        'id': paper['paper_id'],
        'title': paper['title'],
        'keywords': paper['keywords'],
        'abstract': paper['abstract'],
        'authors': paper['authors'],
        'publisher': paper['publisher'],
        'topic_name': paper['topic_name'],
        'year': paper['year'],
        'volumn': paper['volumn'],
        'page_start': paper['page_start'],
        'page_end': paper['page_end'],
    }


def gen_author(author):
    return {
        'author_name': author[0],
    }

def gen_suggest_author(author):
     return {
        'author_name': author['author_name'],
    }

def gen_topic(topic):
    return {
        'toppic_name': topic['topic_name'],
    }


if __name__ == "__main__":
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(debug=True)

import os
from json import dumps
from flask import Flask, render_template, Response, g, request
from neo4j import GraphDatabase, basic_auth
import logging
app = Flask(__name__)
database = os.getenv("NEO4J_DATABASE", "TESTDB")
driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "123"))
session = driver.session()

neo4jVersion = os.getenv("NEO4J_VERSION", "")

def get_db():
    if not hasattr(g, 'neo4j_db'):
        g.neo4j_db = driver.session()
    return g.neo4j_db


@app.route("/graph")
def get_graph():
    try:
        q = request.args["q"]
    except KeyError:
        return []
    db = get_db()
    results = db.read_transaction(lambda tx: list(tx.run("MATCH (au:Author)-[c:COMPOSED]-> (p:Paper)-[r:Cities]->"
                                                         " (p2:Paper) WHERE p.title =~ $title  RETURN p, r , "
                                                         "collect(au.author_name) as author, "
                                                         " collect(p2.title) as citation LIMIT 100",
                                                         {"title": "(?i).*" + q + ".*"}
                                                         )))
    nodes = []
    rels = []
    i = 0
    for record in results:
        paper = record[0]
        nodes.append({"title": paper["title"], "label": "paper"})
        target = i
        i += 1
        for name in record[3]:
            actor = {"title": name, "label": "author"}
            try:
                source = nodes.index(actor)
            except ValueError:
                nodes.append(actor)
                source = i
                i += 1
            rels.append({"source": source, "target": target})
    return Response(dumps({"nodes": nodes, "links": rels}),
                    mimetype="application/json")


@app.route("/search")
def get_search():
    try:
        q = request.args["q"]
    except KeyError:
        return []
    else:
        db = get_db()
        results = db.read_transaction(lambda tx: list(tx.run("MATCH (p:Paper) "
                                                             "WHERE p.title =~ $title "
                                                             "RETURN p", {"title": "(?i).*" + q + ".*"}
                                                             )))
        return Response(dumps([serialize_movie(record['p']) for record in results]),
                        mimetype="application/json")


@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'neo4j_db'):
        g.neo4j_db.close()


@app.route("/")
def main():
    return render_template('index.html')


def serialize_movie(paper):
    return {
        'id': paper['id'],
        'title': paper['title'],
        'keywords': paper['keywords'],
        'abstract': paper['abstract'],
        'authors': paper['authors'],
        'publisher': paper['publisher'],
        'topic_name': paper['topic_name'],
    }


def serialize_cast(author):
    return {
        'name': author[0],
    }


@app.route("/paper/<title>")
def get_movie(title):
    db = get_db()
    result = db.read_transaction(lambda tx: tx.run("MATCH (p:Paper {title:$title}) "
                                                   "OPTIONAL MATCH (p)<-[r]-(author:Author) "
                                                   "RETURN p.title as title,"
                                                   "COLLECT([author.author_name, "
                                                   "HEAD(SPLIT(TOLOWER(TYPE(r)), '_')), r.roles]) AS author "
                                                   "LIMIT 1", {"title": title}).single())

    return Response(dumps({"title": result['title'],
                           "author": [serialize_cast(member)
                                    for member in result['author']]}),
                    mimetype="application/json")



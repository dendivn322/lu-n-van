"""
This script runs the CITATION application using a development server.
"""

from os import environ
from CITATION import app

if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(HOST, PORT)
